<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $fs_nama = $request->fs_name;
        $ls_nama = $request->ls_name;
        
        return view('welcome', compact('fs_nama','ls_nama'));
    }
}
