<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="fs_name"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="ls_name"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="Ml">Male<br>
        <input type="radio" name="Fm">Female<br>
        <input type="radio" name="Oth">Other<br><br>

        <label>Nationaly:</label><br><br>
            <select>
                <option value="ID">Indonesian</option><br><br>
                <option value="EN">English</option><br><br>
                <option value="US">American</option><br><br>
                <option value="Oth">Other</option><br><br>

            </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="ID">Bahasa Indonesia<br>
        <input type="checkbox" name="EN">English<br>
        <input type="checkbox" name="JP">Japanese<br>
        <input type="checkbox" name="Oth">Other<br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up"><br><br>
    </form>
</body>
</html>